from django.shortcuts import render, redirect
from django.forms import ModelForm, Textarea
from django.template import RequestContext
from musiques.models import *
from django import forms
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView

# Morceaux #################################################################################


class MorceauCreateView(CreateView):
    '''
     ajout d'un morceau
    '''
    model = Morceau
    fields = ['titre', 'artiste', 'date_sortie']


class ListeMorceauView(ListView):
    '''
    liste des morceaux
    '''
    model = Morceau

    def get_context_data(self, **kwargs):
        context = super(ListeMorceauView, self).get_context_data(**kwargs)
        context["morceaux"] = Morceau.objects.all()
        return context


def morceau_detail(request, pk):
    return HttpResponse('OK')


class MorceauDetailView(DetailView):
    '''
    détail d'un morceau
    '''
    model = Morceau


class MorceauDelete(DeleteView):
    model = Morceau
    success_url = "/gestion_musiques/morceaux"
    # reverse_lazy('morceau_list')


class MorceauUpdate(UpdateView):
    """
    Modification d'un Morceau
    """
    model = Morceau
    fields = ['titre', 'artiste', 'date_sortie']
    template_name_suffix = '_update_form'


def deleteMorceau(request):
    """
    supprime tous les morceaux
    """
    Morceau.objects.all().delete()

    context = {}

    return render(request, 'musiques/morceau_list.html', context)


# Artistes #################################################################################

class ArtisteCreateView(CreateView):
    '''
     ajout d'un artiste
    '''
    model = Artiste
    fields = ['nom']


class ListeArtisteView(ListView):
    '''
    liste des artistes
    '''
    model = Artiste

    def get_context_data(self, **kwargs):
        context = super(ListeArtisteView, self).get_context_data(**kwargs)
        context["artistes"] = Artiste.objects.all()
        return context


def artiste_detail(request, pk):
    return HttpResponse('OK')


class ArtisteDetailView(DetailView):
    '''
    détail d'un artiste
    '''
    model = Artiste


class ArtisteDelete(DeleteView):
    model = Artiste
    success_url = "/gestion_musiques/artistes"


class ArtisteUpdate(UpdateView):
    """
    Modification d'un Artiste
    """
    model = Artiste
    fields = ['nom']
    template_name_suffix = '_update_form'


def home(request):
    return render(request, 'musiques/gestion_musiques.html')


def deleteArtiste(request):
    """
    supprime tous les artistes
    """
    Artiste.objects.all().delete()

    context = {}

    return render(request, 'musiques/artiste_list.html', context)
