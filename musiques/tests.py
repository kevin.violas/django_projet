from django.urls import reverse
from django.test import TestCase
from django.urls.exceptions import NoReverseMatch
from musiques.models import Morceau, Artiste
from musiques.views import *


class MorceauTestCase(TestCase):
    def setUp(self):
        artiste1 = Artiste.objects.create(nom="artiste1")
        Morceau.objects.create(titre='musique1', artiste=artiste1)
        # Morceau.objects.create(titre='musique2', artiste='artise2')
        # Morceau.objects.create(titre='musique3', artiste='artise3')

    def test_morceau_url_name(self):
        try:
            url = reverse('musiques:morceau_detail', args=[
                          Morceau.objects.get(titre="musique1").pk])
        except NoReverseMatch:
            assert False

    def test_morceau_url(self):
        morceau = Morceau.objects.get(titre='musique1')
        url = reverse('musiques:morceau_detail', args=[
                      Morceau.objects.get(titre="musique1").pk])
        response = self.client.get(url)
        assert response.status_code == 200


class ArtisteTestCase(TestCase):
    def setUp(self):
        Artiste.objects.create(nom='artiste1')
        Artiste.objects.create(nom='artiste2')
        Artiste.objects.create(nom='artiste3')

    def test_artiste_url_name(self):
        try:
            url = reverse('musiques:artiste_detail', args=[
                          Artiste.objects.get(nom='artiste1').pk])
        except NoReverseMatch:
            assert False

    def test_artiste_url(self):
        artiste = Artiste.objects.get(nom='artiste1')
        url = reverse('musiques:artiste_detail', args=[
                      Artiste.objects.get(nom='artiste1').pk])
        response = self.client.get(url)
        assert response.status_code == 200

    def test_artiste_liste(self):
        url = reverse('musiques:artiste_list')
        response = self.client.get(url)
        assert response.status_code == 200


from django.test import RequestFactory


class ArtisteFormTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_post(self):
        data = {
            'nom': 'linkin park'
        }
        request = self.factory.post(reverse('musiques:artiste_form'), data)
        response = ArtisteCreateView.as_view()(request)
        self.assertEqual(response.status_code, 302)
        # ajout effectué dans la base
        assert Artiste.objects.get(nom="linkin park") != None


class MorceauFomTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_post(self):
        artiste = Artiste.objects.create(nom="linkin park")
        data = {
            'titre': 'New divide',
            'date_sortie': '10/10/2014',
            'artiste': artiste.pk
        }
        request = self.factory.post(reverse('musiques:morceau_form'), data)
        response = MorceauCreateView.as_view()(request)
        self.assertEqual(response.status_code, 302)
        assert Morceau.objects.get(titre="New divide") != None
