from django.conf.urls import url
from django.views.generic import TemplateView
from .views import *
from . import views

app_name = 'musiques'
urlpatterns = [
    url(r'^$', views.home, name="gestion_musiques"),
    url(r'^morceaux/(?P<pk>\d+)$',
        MorceauDetailView.as_view(), name='morceau_detail'),
    url(r'^morceaux/add/$', MorceauCreateView.as_view(),
        name="morceau_form"),
    url(r'^morceaux/delete/(?P<pk>\d+)$', MorceauDelete.as_view(),
        name="morceau_delete"),
    url(r'^morceaux/update/(?P<pk>\d+)$', MorceauUpdate.as_view(),
        name="morceau_update"),
    url(r'^morceaux/$', ListeMorceauView.as_view(), name='morceau_list'),
    url(r'^morceaux/deleteall/$', views.deleteMorceau, name="morceau_deleteall"),

    url(r'^artistes/(?P<pk>\d+)$',
        ArtisteDetailView.as_view(), name='artiste_detail'),
    url(r'^artistes/$', ListeArtisteView.as_view(), name='artiste_list'),
    url(r'^artistes/add/$', ArtisteCreateView.as_view(),
        name="artiste_form"),
    url(r'^artistes/delete/(?P<pk>\d+)$', ArtisteDelete.as_view(),
        name="artiste_delete"),
    url(r'^artistes/update/(?P<pk>\d+)$', ArtisteUpdate.as_view(),
        name="artiste_update"),
    url(r'^artistes/deleteall/$', views.deleteArtiste,
        name="artiste_deleteall")
]
